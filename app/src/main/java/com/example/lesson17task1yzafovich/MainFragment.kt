package com.example.lesson17task1yzafovich

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import java.util.ArrayList
import java.util.Random

class MainFragment : Fragment() {

    private var recyclerViewAdapter: Adapter? = null
    private var recyclerView: RecyclerView? = null

    private val menu: Menu? = null

    private var urlList: MutableList<String>? = null
    private val titleList: List<String>? = null
    private var textList: MutableList<String>? = null

    private var list: MutableList<Data>? = null
    private var newList: MutableList<Data>? = null

    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    private var layoutManager: RecyclerView.LayoutManager? = null
    internal var x = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        setHasOptionsMenu(true)

        val v = inflater.inflate(R.layout.recycler_view_fragment, container, false)

        layoutManager = LinearLayoutManager(context)

        list = initList()
        recyclerViewAdapter = Adapter(list!!)
        recyclerView = v.findViewById(R.id.recycler_view)
        recyclerView!!.layoutManager = layoutManager
        recyclerView!!.adapter = recyclerViewAdapter

        //        recyclerView.setLayoutManager(layoutManager);
        //        recyclerView.setAdapter(recyclerViewAdapter);

        swipeRefreshLayout = v.findViewById(R.id.swipe_to_refresh)
        swipeRefreshLayout!!.setOnRefreshListener {
            refresh()
            swipeRefreshLayout!!.isRefreshing = false
        }


        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                val customDialog = CustomDialog(
                        getString(R.string.call_dialog_title),
                        getString(R.string.call_dialog_message)
                )

                val bundle = Bundle()

                bundle.putSerializable(CUSTOM_DIALOG_KEY, object : CustomDialog.CallTrigger {
                    override fun triggerYes() {
                        val position = viewHolder.adapterPosition
                        newList = ArrayList(list!!)
                        newList!!.removeAt(position)
                        recyclerViewAdapter!!.updateDataListItem(newList!!)

                    }

                    override fun triggerNo() {
                        val position = viewHolder.adapterPosition
                        recyclerViewAdapter!!.notifyItemChanged(position)
                    }
                })

                customDialog.arguments = bundle

                customDialog.show(fragmentManager!!, "show")
            }
        }).attachToRecyclerView(recyclerView)
        return v
    }

    private fun generateText(): String {

        val words = "is has great communication and excellent response time! Will stay again The apartment is all 2 people need for a comfortable stay, and the location is pretty great! I have 2 complaints....... the batteries were dead in the remote for the TiVo so we spent what two days we were there without any TV. Which is ok I guess but we were told the host would be coming by to put batteries in the remote and NO ONE SHOWED UP OR RESPONDED. But the icing on the cake was the fact the shower shut down MID SHOWER this morning and I had to rinse my hair in the sink. That’s understandable, I guess, except when I reach out to the host I got absolutely ZERO RESPONSE. I emailed through Airbnb, called and texted and NOTHING. It’s hot and humid and no way we could go without a shower. After a few hours with no response, and no working shower, we reach out to Airbnb customer support and they were able to get us into another Airbnb. So the apartment and location again are great, but the customer service is shit!"
        val wordsArray = words.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()


        val size = Random().nextInt(20)
        textList = ArrayList()


        for (i in 0 until size) {
            val index = Random().nextInt(wordsArray.size)
            val randomWord = wordsArray[index]

            textList!!.add(randomWord)
        }

        var str = String()
        for (i in textList!!.indices) {
            str = str + " " + textList!![i]
        }

        return str
    }


    private fun generateTitle(): String {
        val words = "is has great communication and excellent response time! Will stay again The apartment is all 2 people need for a comfortable stay, and the location is pretty great! I have 2 complaints....... the batteries were dead in the remote for the TiVo so we spent what two days we were there without any TV. Which is ok I guess but we were told the host would be coming by to put batteries in the remote and NO ONE SHOWED UP OR RESPONDED. But the icing on the cake was the fact the shower shut down MID SHOWER this morning and I had to rinse my hair in the sink. That’s understandable, I guess, except when I reach out to the host I got absolutely ZERO RESPONSE. I emailed through Airbnb, called and texted and NOTHING. It’s hot and humid and no way we could go without a shower. After a few hours with no response, and no working shower, we reach out to Airbnb customer support and they were able to get us into another Airbnb. So the apartment and location again are great, but the customer service is shit!"
        val wordsArray = words.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val random = Random().nextInt(wordsArray.size)

        return wordsArray[random]
    }


    private fun generateURL(): String {
        urlList = ArrayList()

        urlList!!.add("https://a0.muscach e.com/im/pictures/56bff280-aba3-42f3-af42-adc2814a72f4.jpg?aki_policy=large")
        urlList!!.add("https://a0.muscache.com/im/pictures/45680811/f4987a12_original.jpg?aki_policy=large")
        urlList!!.add("https://a0.muscache.com/im/pictures/140333/3ab8f121_original.jpg?aki_policy=large")
        urlList!!.add("https://a0.muscache.com/im/pictures/812e040f-6f6f-4cae-ad67-66c050b57c1f.jpg?aki_policy=large")

        val item = Random().nextInt(urlList!!.size)

        return urlList!![item]
    }


    private fun changeLayoutManager(x: Boolean): RecyclerView.LayoutManager? {

        if (x) {
            layoutManager = GridLayoutManager(context, 2)
            return layoutManager
        } else {
            layoutManager = LinearLayoutManager(context)
            return layoutManager
        }

    }

    private fun refresh() {
        newList = ArrayList(list!!)
        recyclerViewAdapter!!.updateDataListItem(newList!!)
        swipeRefreshLayout!!.isRefreshing = false
    }


    fun initList(): MutableList<Data> {
        list = mutableListOf()

        list!!.add(Data(generateTitle(), generateText(), generateURL()))
        list!!.add(Data(generateTitle(), generateText(), generateURL()))
        list!!.add(Data(generateTitle(), generateText(), generateURL()))
        list!!.add(Data(generateTitle(), generateText(), generateURL()))
        return list as MutableList<Data>
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.menu_title_add -> {
                newList = ArrayList(list!!)
                newList!!.add(Data(generateTitle(), generateText(), generateURL()))
                recyclerViewAdapter!!.updateDataListItem(newList!!)
            }

            R.id.menu_title_layout_style -> {
                layoutManager = changeLayoutManager(x)
                recyclerView!!.layoutManager = layoutManager
                recyclerView!!.setHasFixedSize(true)
                recyclerView!!.adapter = recyclerViewAdapter
                //                menu.getItem(R.id.menu_title_layout_style).setIcon(R.drawable.ic_view_headline_black_24dp);
                //
                ////                menu.findItem(R.id.menu_title_layout_style).setIcon(R.drawable.ic_view_headline_black_24dp);
                //
                x = !x
            }
        }
        //        menu.findItem(R.id.menu_title_add);
        return super.onOptionsItemSelected(item)
    }

    companion object {

        private val CUSTOM_DIALOG_KEY = "CUSTOM DIALOG"
    }
}
