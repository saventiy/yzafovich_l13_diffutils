//package com.example.lesson17task1yzafovich;
//
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.fragment.app.Fragment;
//import androidx.recyclerview.widget.GridLayoutManager;
//import androidx.recyclerview.widget.ItemTouchHelper;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Random;
//
//public class MainFragment extends Fragment {
//
//    private static final String CUSTOM_DIALOG_KEY = "CUSTOM DIALOG";
//
//    private Adapter recyclerViewAdapter;
//    private RecyclerView recyclerView;
//
//    private Menu menu;
//
//    private List<String> urlList;
//    private List<String> titleList;
//    private List<String> textList;
//
//    private List<Data> list;
//    private List<Data> newList;
//
//    private SwipeRefreshLayout swipeRefreshLayout;
//
//    private RecyclerView.LayoutManager layoutManager;
//    boolean x = true;
//
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }
//
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//
//        setHasOptionsMenu(true);
//
//        View v = inflater.inflate(R.layout.recycler_view_fragment, container, false);
//
//        layoutManager = new LinearLayoutManager(getContext());
//
//        list = initList();
//        recyclerViewAdapter = new Adapter(list);
//        recyclerView = v.findViewById(R.id.recycler_view);
//        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setAdapter(recyclerViewAdapter);
//
////        recyclerView.setLayoutManager(layoutManager);
////        recyclerView.setAdapter(recyclerViewAdapter);
//
//        swipeRefreshLayout = v.findViewById(R.id.swipe_to_refresh);
//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                refresh();
//                swipeRefreshLayout.setRefreshing(false);
//            }
//        });
//
//
//        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
//
//            @Override
//            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
//                return false;
//            }
//
//            @Override
//            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int direction) {
//
//                CustomDialog customDialog = new CustomDialog(
//                        getString(R.string.call_dialog_title),
//                        getString(R.string.call_dialog_message)
//                );
//
//                Bundle bundle = new Bundle();
//
//                bundle.putSerializable(CUSTOM_DIALOG_KEY, new CustomDialog.CallTrigger() {
//                    @Override
//                    public Void triggerYes() {
//                        int position = viewHolder.getAdapterPosition();
//                        newList = new ArrayList<>(list);
//                        newList.remove(position);
//                        recyclerViewAdapter.updateDataListItem(newList);
//                        return null;
//                    }
//
//                    @Override
//                    public Void triggerNo() {
//                        int position = viewHolder.getAdapterPosition();
//                        recyclerViewAdapter.notifyItemChanged(position);
//                        return null;
//                    }
//                });
//
//                customDialog.setArguments(bundle);
//
//                customDialog.show(getFragmentManager(), "show");
//            }
//        }).attachToRecyclerView(recyclerView);
//        return v;
//    }
//
//    private String generateText() {
//
//        String words = "is has great communication and excellent response time! Will stay again The apartment is all 2 people need for a comfortable stay, and the location is pretty great! I have 2 complaints....... the batteries were dead in the remote for the TiVo so we spent what two days we were there without any TV. Which is ok I guess but we were told the host would be coming by to put batteries in the remote and NO ONE SHOWED UP OR RESPONDED. But the icing on the cake was the fact the shower shut down MID SHOWER this morning and I had to rinse my hair in the sink. That’s understandable, I guess, except when I reach out to the host I got absolutely ZERO RESPONSE. I emailed through Airbnb, called and texted and NOTHING. It’s hot and humid and no way we could go without a shower. After a few hours with no response, and no working shower, we reach out to Airbnb customer support and they were able to get us into another Airbnb. So the apartment and location again are great, but the customer service is shit!";
//        String[] wordsArray = words.split(" ");
//
//
//        int size = new Random().nextInt(20);
//        textList = new ArrayList<>();
//
//
//        for (int i = 0; i != size; i++) {
//            int index = new Random().nextInt(wordsArray.length);
//            String randomWord = wordsArray[index];
//
//            textList.add(randomWord);
//        }
//
//        String str = new String();
//        for (int i = 0; i != textList.size(); ++i) {
//            str = str + " " + textList.get(i);
//        }
//
//        return str;
//    }
//
//
//    private String generateTitle() {
//        String words = "is has great communication and excellent response time! Will stay again The apartment is all 2 people need for a comfortable stay, and the location is pretty great! I have 2 complaints....... the batteries were dead in the remote for the TiVo so we spent what two days we were there without any TV. Which is ok I guess but we were told the host would be coming by to put batteries in the remote and NO ONE SHOWED UP OR RESPONDED. But the icing on the cake was the fact the shower shut down MID SHOWER this morning and I had to rinse my hair in the sink. That’s understandable, I guess, except when I reach out to the host I got absolutely ZERO RESPONSE. I emailed through Airbnb, called and texted and NOTHING. It’s hot and humid and no way we could go without a shower. After a few hours with no response, and no working shower, we reach out to Airbnb customer support and they were able to get us into another Airbnb. So the apartment and location again are great, but the customer service is shit!";
//        String[] wordsArray = words.split(" ");
//
//        int random = new Random().nextInt(wordsArray.length);
//
//        return wordsArray[random];
//    }
//
//
//    private String generateURL() {
//        urlList = new ArrayList<>();
//
//        urlList.add("https://a0.muscach e.com/im/pictures/56bff280-aba3-42f3-af42-adc2814a72f4.jpg?aki_policy=large");
//        urlList.add("https://a0.muscache.com/im/pictures/45680811/f4987a12_original.jpg?aki_policy=large");
//        urlList.add("https://a0.muscache.com/im/pictures/140333/3ab8f121_original.jpg?aki_policy=large");
//        urlList.add("https://a0.muscache.com/im/pictures/812e040f-6f6f-4cae-ad67-66c050b57c1f.jpg?aki_policy=large");
//
//        int item = new Random().nextInt(urlList.size());
//
//        return urlList.get(item);
//    }
//
//
//    private RecyclerView.LayoutManager changeLayoutManager(boolean x) {
//
//        if (x) {
//            layoutManager = new GridLayoutManager(getContext(), 2);
//            return layoutManager;
//        } else {
//            layoutManager = new LinearLayoutManager(getContext());
//            return layoutManager;
//        }
//
//    }
//
//    private void refresh() {
//        newList = new ArrayList<>(list);
//        recyclerViewAdapter.updateDataListItem(newList);
//        swipeRefreshLayout.setRefreshing(false);
//    }
//
//
//    public List<Data> initList() {
//        list = new ArrayList<>();
//
//        list.add(new Data(generateTitle(), generateText(), generateURL()));
//        list.add(new Data(generateTitle(), generateText(), generateURL()));
//        list.add(new Data(generateTitle(), generateText(), generateURL()));
//        list.add(new Data(generateTitle(), generateText(), generateURL()));
//
//        return list;
//    }
//
//
//    @Override
//    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
//        inflater.inflate(R.menu.menu, menu);
//
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//
//
//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//
//        switch (item.getItemId()) {
//            case R.id.menu_title_add:
//                newList = new ArrayList<>(list);
//                newList.add(new Data(generateTitle(), generateText(), generateURL()));
//                recyclerViewAdapter.updateDataListItem(newList);
//                break;
//
//            case R.id.menu_title_layout_style:
//                layoutManager = changeLayoutManager(x);
//                recyclerView.setLayoutManager(layoutManager);
//                recyclerView.setHasFixedSize(true);
//                recyclerView.setAdapter(recyclerViewAdapter);
////                menu.getItem(R.id.menu_title_layout_style).setIcon(R.drawable.ic_view_headline_black_24dp);
////
//////                menu.findItem(R.id.menu_title_layout_style).setIcon(R.drawable.ic_view_headline_black_24dp);
////
//                x = !x;
//
//                break;
//        }
////        menu.findItem(R.id.menu_title_add);
//        return super.onOptionsItemSelected(item);
//    }
//}
