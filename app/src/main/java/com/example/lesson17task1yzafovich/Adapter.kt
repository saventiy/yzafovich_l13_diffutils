package com.example.lesson17task1yzafovich

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_item.view.*

class Adapter(val list: MutableList<Data>) : RecyclerView.Adapter<Adapter.DataHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.fragment_item, parent, false)

        return DataHolder(v)
    }

    override fun onBindViewHolder(holder: DataHolder, position: Int) {
        val data = list.get(position)

        holder.title.setText(data.title)
        holder.text.setText(data.text)

        Picasso.get().load(list[position].photoURL).into(holder.avatar)
    }

    override fun getItemCount() = list.size


    class DataHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.text_view_title
        val text = itemView.text_view_text
        val avatar = itemView.image_view_avatar
    }

    fun updateDataListItem(data: List<Data>) {
        val diffCallback = DataDiffCallback(this.list, data)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        with(list){
            clear()
            addAll(data)
        }

        diffResult.dispatchUpdatesTo(this)
    }

}