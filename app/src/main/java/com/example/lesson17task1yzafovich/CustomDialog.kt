package com.example.lesson17task1yzafovich

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import java.io.Serializable

class CustomDialog(val title: String, val supportingText: String) : DialogFragment() {
    private val CUSTOM_DIALOG_KEY = "CUSTOM DIALOG"

    private var listener: CallTrigger? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return createDialog()
    }

    private fun createDialog(): Dialog {

        listener = arguments!!.get(CUSTOM_DIALOG_KEY) as CallTrigger?

        return AlertDialog.Builder(requireActivity())
                .setTitle(title)
                .setMessage(supportingText)
                .setPositiveButton("Yes") { dialog, which -> listener?.triggerYes() }
                .setNegativeButton("No") { dialog, which -> listener?.triggerNo() }
                .create()
    }

    interface CallTrigger : Serializable {
        fun triggerYes()
        fun triggerNo()
    }
}