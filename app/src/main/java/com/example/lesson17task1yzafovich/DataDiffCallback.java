package com.example.lesson17task1yzafovich;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

public class DataDiffCallback extends DiffUtil.Callback {


    private List<Data> oldDataList;
    private List<Data> newDataList;


    public DataDiffCallback(List<Data> oldDataList, List<Data> newDataList) {
        this.oldDataList = oldDataList;
        this.newDataList = newDataList;
    }

    @Override
    public int getOldListSize() {
        return oldDataList.size();
    }

    @Override
    public int getNewListSize() {
        return newDataList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldDataList.get(oldItemPosition).getText().equals(newDataList.get(newItemPosition).getText()) ;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Data newData = newDataList.get(newItemPosition);
        Data oldData = oldDataList.get(oldItemPosition);

        return oldData.getText().equals(newData.getText());
    }
}
