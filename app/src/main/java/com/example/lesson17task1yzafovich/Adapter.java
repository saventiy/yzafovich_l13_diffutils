//package com.example.lesson17task1yzafovich;
//
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.DiffUtil;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.squareup.picasso.Picasso;
//
//import java.util.List;
//
//public class Adapter extends RecyclerView.Adapter<Adapter.DataHolder> {
//
//    List<Data> list;
//
//    public Adapter(List<Data> list) {
////        this.list.addAll(list);
//        this.list = list;
//    }
//
//    @NonNull
//    @Override
//    public DataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
////        switch (viewType){
////            case 1: return new DataHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_item, parent, false));
////            case 2: return new DataHolder(LayoutInflater.from(parent.getContext()).inflate())
////        }
//        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_item, parent, false);
//        return new DataHolder(v);
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull DataHolder holder, int position) {
//
//        Data data = list.get(position);
//        holder.title.setText(data.getTitle());
//        holder.text.setText(data.getText());
//
//        Picasso.get().load(list.get(position).getPhotoURL()).into(holder.avatar);
//
//    }
//
//
//    public void updateDataListItem(List<Data> data) {
//        DataDiffCallback diffCallback = new DataDiffCallback(this.list, data);
//        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
//
//        this.list.clear();
//        this.list.addAll(data);
//        diffResult.dispatchUpdatesTo(this);
//    }
//
//    @Override
//    public int getItemCount() {
//        return list.size();
//    }
//
//
//    class DataHolder extends RecyclerView.ViewHolder {
//
//        public TextView title;
//        public TextView text;
//        public ImageView avatar;
//
//        public DataHolder(@NonNull View itemView) {
//            super(itemView);
//
//            title = itemView.findViewById(R.id.text_view_title);
//            text = itemView.findViewById(R.id.text_view_text);
//            avatar = itemView.findViewById(R.id.image_view_avatar);
//        }
//    }
//
//
//}
