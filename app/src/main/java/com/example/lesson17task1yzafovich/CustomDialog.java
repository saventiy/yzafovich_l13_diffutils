//package com.example.lesson17task1yzafovich;
//
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.content.DialogInterface;
//import android.os.Bundle;
//
//import androidx.fragment.app.DialogFragment;
//
//import java.io.Serializable;
//
//public class CustomDialog extends DialogFragment {
//
//
//    private static final String CUSTOM_DIALOG_KEY = "CUSTOM DIALOG";
//
//    private String title;
//    private String supportingText;
//
//    private CallTrigger listener = null;
//
//    public CustomDialog(String title, String supportingText) {
//        this.title = title;
//        this.supportingText = supportingText;
//    }
//
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//
//
//        return createCustomAlertDialog();
//    }
//
//    private Dialog createCustomAlertDialog() {
//
//        listener = (CallTrigger) getArguments().getSerializable(CUSTOM_DIALOG_KEY);
//
//        return new AlertDialog.Builder(requireActivity())
//                .setTitle(title)
//                .setMessage(supportingText)
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        listener.triggerYes();
//                    }
//                })
//                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        listener.triggerNo();
//                    }
//                })
//                .create();
//
//
//    }
//
//
//    public interface CallTrigger extends Serializable {
//        void triggerYes();
//        void triggerNo();
//    }
//
//}
